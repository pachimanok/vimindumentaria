<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PROPUESTA :: VOX INDUMENTRIA ::</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <link rel="stylesheet" href="assets/css/slicknav.css">
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/slick.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/style.css">
</head>
   <body>  
    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img class="img-fluid"src="assets/img/VIM/LOGO.JPG" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <header>
        <!-- Header Start -->
       <div class="header-area">
            <div class="main-header ">
                <div class="header-top top-bg d-none d-lg-block">
                   <div class="container-fluid">
                       <div class="col-xl-12">
                            <div class="row d-flex justify-content-between align-items-center">
                                <div class="header-info-left d-flex">
                                    <!--div class="flag">
                                        <img src="assets/img/icon/header_icon.png" alt="">
                                    </div-->
                                    <ul class="contact-now">     
                                        <li><i class="fa fa-mobile"></i> +261 383 2212 - 261 666 5835</li>
                                    </ul>
                                </div>
                                <div class="header-info-right">
                                   <ul>                                          
                                       <li><a href="login.html">Mi Cuenta </a></li>
                                       <li><a href="product_list.html">lista de deseos  </a></li>
                                       <li><a href="cart.html">Comprar</a></li>
                                       <li><a href="cart.html">Carrito</a></li>
                                       <li><a href="checkout.html">Checkout</a></li>
                                   </ul>
                                </div>
                            </div>
                       </div>
                   </div>
                </div>
               <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-3">
                                <div class="logo">
                                  <a href="index.php"><img src="assets/img/VIM/LOGO.JPG" style="height: 3rem;"alt=""></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-8 col-md-7 col-sm-5">
                                <!-- Main-menu -->
                                <div class="main-menu f-right d-none d-lg-block">
                                    <nav>                                                
                                        <ul id="navigation">                                                                                                                                     
                                            <li><a href="index.html">Inicio</a></li>
                                            <li><a href="Catagori.html">Catalogo</a></li>
                                            <li class="hot"><a href="#">Ofertas</a>
                                                <ul class="submenu">
                                                    <li><a href="product_list.html"> Product list</a></li>
                                                    <li><a href="single-product.html"> Product Details</a></li>
                                                </ul>
                                            </li>
                                            <!--li><a href="blog.html">Blog</a>
                                                <ul class="submenu">
                                                    <li><a href="blog.html">Blog</a></li>
                                                    <li><a href="single-blog.html">Blog Details</a></li>
                                                </ul>
                                            </li-->
                                            <!--li><a href="#">Pages</a>
                                                <ul class="submenu">
                                                    <li><a href="login.html">Login</a></li>
                                                    <li><a href="cart.html">Card</a></li>
                                                    <li><a href="elements.html">Element</a></li>
                                                    <li><a href="about.html">About</a></li>
                                                    <li><a href="confirmation.html">Confirmation</a></li>
                                                    <li><a href="cart.html">Shopping Cart</a></li>
                                                    <li><a href="checkout.html">Product Checkout</a></li>
                                                </ul-->
                                            </li>
                                            <li><a href="contact.html">Contacto</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div> 
                            <div class="col-xl-5 col-lg-3 col-md-3 col-sm-3 fix-card">
                                <ul class="header-right f-right d-none d-lg-block d-flex justify-content-between">
                                    <!--li class="d-none d-xl-block">
                                        <div class="form-box f-right ">
                                            <input type="text" name="Search" placeholder="Search products">
                                            <div class="search-icon">
                                                <i class="fas fa-search special-tag"></i>
                                            </div>
                                        </div>
                                     </li-->
                                    <li class=" d-none d-xl-block">
                                        <div class="favorit-items">
                                            <i class="far fa-heart"></i>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="shopping-card">
                                            <a href="cart.html"><i class="fas fa-shopping-cart"></i></a>
                                        </div>
                                    </li>
                                   <li class="d-none d-lg-block"> <a href="#" class="btn header-btn">Sign in</a></li>
                                </ul>
                            </div>
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
       </div>
        <!-- Header End -->
    </header>

    <main>

        <!-- slider Area Start-->
        <div class="slider-area ">
            <!-- Mobile Menu -->
            <div class="single-slider slider-height2 d-flex align-items-center" data-background="assets/img/VIM/fondocatalogo.JPG">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12" style="text-align: center;">
                            <img class="img-fluid" src="assets/img/VIM/catalogo.JPG"  alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->

        <!-- Latest Products Start -->
        <section class="latest-product-area latest-padding">
            <div class="container">
                <div class="row product-btn d-flex justify-content-between">
                        <div class="properties__button">
                            <!--Nav Button  -->
                            <nav>                                                                                                
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-controls="nav-all" aria-selected="true">Todo</a>
                                    <a class="nav-item nav-link" id="nav-calza-tab" data-toggle="tab" href="#nav-calza" role="tab" aria-controls="nav-calza" aria-selected="false">Calzas</a>
                                    <a class="nav-item nav-link" id="nav-top-tab" data-toggle="tab" href="#nav-top" role="tab" aria-controls="nav-top" aria-selected="false">Tops</a>
                                    <a class="nav-item nav-link" id="nav-short-tab" data-toggle="tab" href="#nav-short" role="tab" aria-controls="nav-short" aria-selected="false">Short</a>
                                    <a class="nav-item nav-link" id="nav-jogger-tab" data-toggle="tab" href="#nav-jogger" role="tab" aria-controls="nav-jogger" aria-selected="false">Jogger</a>
                                    <a class="nav-item nav-link" id="nav-remera-tab" data-toggle="tab" href="#nav-remera" role="tab" aria-controls="nav-remera" aria-selected="false">Remera / Musculosa</a>
                                    <a class="nav-item nav-link" id="nav-campera-tab" data-toggle="tab" href="#nav-campera" role="tab" aria-controls="nav-campera" aria-selected="false">Camparas</a>
                                    <a class="nav-item nav-link" id="nav-accesorio-tab" data-toggle="tab" href="#nav-accesorio" role="tab" aria-controls="nav-accesorio" aria-selected="false">Accesorios</a>
                                </div>
                            </nav>
                            <!--End Nav Button  -->
                        </div>
                        <!--div class="select-this d-flex">
                            <div class="featured">
                                <span>Short by: </span>
                            </div>
                            <form action="#">
                                <div class="select-itms">
                                    <select name="select" id="select1">
                                        <option value="">Featured</option>
                                        <option value="">Featured A</option>
                                        <option value="">Featured B</option>
                                        <option value="">Featured C</option>
                                    </select>
                                </div>
                            </form>
                        </div-->
                </div>
                <!-- Nav Card -->
                <div class="tab-content" id="nav-tabContent">
                    
                    <!-- card All -->
                    <div class="tab-pane fade show active" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">
                        <div class="row">

                            <?php
                           
                           include('db.php');


                            $query = "SELECT * FROM `catalogo` GROUP BY `title`";
                            $result= mysqli_query($conn, $query);  
                    
                            while($row = mysqli_fetch_assoc($result)) { 
                            
                            $id = $row['id'];
                            $title= $row['title'];
                            $description = $row['description'];
                            $imagen = $row['imagen'];
                            $precio_venta = $row['precio_venta'];
                            $oferta = $row['oferta'];
                            $rebaja_ok = $row['rebaja_ok'];
                            $type = $row['type'];

                            ?>
                            <div class="col-xl-4 col-lg-4 col-md-6">
                                <div class="single-product mb-60">
                                    <div class="product-img">
                                        <img src="<?php echo $imagen;?>" alt="">  
                                    </div>
                                    <div class="product-caption">
                                        <!--div class="product-ratting">
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star low-star"></i>
                                            <i class="far fa-star low-star"></i>
                                        </div-->
                                        <h4><a href="#"><?php echo $title;?></a></h4>
                                        <div class="price">
                                            <ul>
                                                <li>$<?php echo $precio_venta;?></li>                                         
                                                <!--li class="discount">$60.00</li-->
                                            </ul>

                                        </div>
                                            <a class="genric-btn primary" href="actions/carrito.php?id=<?php echo $id;?>">+ Carrito</a>
                                            <a class="genric-btn primary-border" href="../actions/comprar.php?id=<?php echo $id;?>">comprar</a>
                                    </div>
                                </div>
                            </div>
                            <?php }

                            ?>
                        </div>
                    </div>

                  
                    <!-- Card calzas -->
                    <div class="tab-pane fade" id="nav-calza" role="tabpanel" aria-labelledby="nav-calza-tab">
                        <div class="row">
                        <?php
                           
                         


                           $query = "SELECT * FROM `catalogo` WHERE `type` = 'calza' GROUP BY `title`";
                           $result= mysqli_query($conn, $query);  
                   
                           while($row = mysqli_fetch_assoc($result)) { 
                           
                           $id = $row['id'];
                           $title= $row['title'];
                           $description = $row['description'];
                           $imagen = $row['imagen'];
                           $precio_venta = $row['precio_venta'];
                           $oferta = $row['oferta'];
                           $rebaja_ok = $row['rebaja_ok'];
                           $type = $row['type'];
                       ?>
                           <div class="col-xl-4 col-lg-4 col-md-6">
                               <div class="single-product mb-60">
                                   <div class="product-img">
                                       <img src="<?php echo $imagen;?>" alt="">
                                       
                                   </div>
                                   <div class="product-caption">
                                       <!--div class="product-ratting">
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star low-star"></i>
                                           <i class="far fa-star low-star"></i>
                                       </div-->
                                       <h4><a href="#"><?php echo $title;?></a></h4>
                                       <div class="price">
                                           <ul>
                                               <li>$<?php echo $precio_venta;?></li>
                                               <!--li class="discount">$60.00</li-->
                                           </ul>
                                       </div>
                                       <a class="genric-btn primary" href="actions/carrito.php?id=<?php echo $id;?>">+ Carrito</a>
                                        <a class="genric-btn primary-border" href="#">comprar</a>
                                   </div>
                               </div>
                           </div>
                           <?php }

                           ?>
                        </div>
                    </div>
                    <!-- Card Tops -->
                    <div class="tab-pane fade" id="nav-top" role="tabpanel" aria-labelledby="nav-top-tab">
                        <div class="row">
                        <?php
                           
                           

                           $query = "SELECT * FROM `catalogo` WHERE `type` = 'top' GROUP BY `title`";
                           $result= mysqli_query($conn, $query);  
                   
                           while($row = mysqli_fetch_assoc($result)) { 
                           
                           $id = $row['id'];
                           $title= $row['title'];
                           $description = $row['description'];
                           $imagen = $row['imagen'];
                           $precio_venta = $row['precio_venta'];
                           $oferta = $row['oferta'];
                           $rebaja_ok = $row['rebaja_ok'];
                           $type = $row['type'];
                       ?>
                           <div class="col-xl-4 col-lg-4 col-md-6">
                               <div class="single-product mb-60">
                                   <div class="product-img">
                                       <img class="img-fluid"src="<?php echo $imagen;?>" alt="">
                                       
                                   </div>
                                   <div class="product-caption">
                                       <!--div class="product-ratting">
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star low-star"></i>
                                           <i class="far fa-star low-star"></i>
                                       </div-->
                                       <h4><a href="#"><?php echo $title;?></a></h4>
                                       <div class="price">
                                           <ul>
                                               <li>$<?php echo $precio_venta;?></li>
                                               <!--li class="discount">$60.00</li-->
                                           </ul>
                                       </div>
                                       <a class="genric-btn primary" href="actions/carrito.php?id=<?php echo $id;?>">+ Carrito</a>
                                        <a class="genric-btn primary-border" href="#">comprar</a>
                                   </div>
                               </div>
                           </div>
                           <?php }
                           ?>
                        </div>
                    </div>
                     <!-- Card Short -->
                     <div class="tab-pane fade" id="nav-short" role="tabpanel" aria-labelledby="nav-short-tab">
                        <div class="row">
                        <?php
                           
                           
                           $query = "SELECT * FROM `catalogo` WHERE `type` = 'short' GROUP BY `title`";
                           $result= mysqli_query($conn, $query);  
                   
                           while($row = mysqli_fetch_assoc($result)) { 
                           
                           $id = $row['id'];
                           $title= $row['title'];
                           $description = $row['description'];
                           $imagen = $row['imagen'];
                           $precio_venta = $row['precio_venta'];
                           $oferta = $row['oferta'];
                           $rebaja_ok = $row['rebaja_ok'];
                           $type = $row['type'];
                       ?>
                           <div class="col-xl-4 col-lg-4 col-md-6">
                               <div class="single-product mb-60">
                                   <div class="product-img">
                                       <img src="<?php echo $imagen;?>" alt="">
                                       
                                   </div>
                                   <div class="product-caption">
                                       <!--div class="product-ratting">
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star low-star"></i>
                                           <i class="far fa-star low-star"></i>
                                       </div-->
                                       <h4><a href="#"><?php echo $title;?></a></h4>
                                       <div class="price">
                                           <ul>
                                               <li>$<?php echo $precio_venta;?></li>
                                               <!--li class="discount">$60.00</li-->
                                           </ul>
                                       </div>
                                       <a class="genric-btn primary" href="actions/carrito.php?id=<?php echo $id;?>">+ Carrito</a>
                                        <a class="genric-btn primary-border" href="#">comprar</a>
                                   </div>
                               </div>
                           </div>
                           <?php }

                           ?>
                        </div>
                    </div>
                    <!-- card Jogger-->
                    <div class="tab-pane fade" id="nav-jogger" role="tabpanel" aria-labelledby="nav-jogger-tab">
                        <div class="row">
                        <?php
                           
                           
                           $query = "SELECT * FROM `catalogo` WHERE `type` = 'jogger' GROUP BY `title`";
                           $result= mysqli_query($conn, $query);  
                   
                           while($row = mysqli_fetch_assoc($result)) { 
                           
                           $id = $row['id'];
                           $title= $row['title'];
                           $description = $row['description'];
                           $imagen = $row['imagen'];
                           $precio_venta = $row['precio_venta'];
                           $oferta = $row['oferta'];
                           $rebaja_ok = $row['rebaja_ok'];
                           $type = $row['type'];
                       ?>
                           <div class="col-xl-4 col-lg-4 col-md-6">
                               <div class="single-product mb-60">
                                   <div class="product-img">
                                       <img src="<?php echo $imagen;?>" alt="">
                                       
                                   </div>
                                   <div class="product-caption">
                                       <!--div class="product-ratting">
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star low-star"></i>
                                           <i class="far fa-star low-star"></i>
                                       </div-->
                                       <h4><a href="#"><?php echo $title;?></a></h4>
                                       <div class="price">
                                           <ul>
                                               <li>$<?php echo $precio_venta;?></li>
                                               <!--li class="discount">$60.00</li-->
                                           </ul>
                                       </div>
                                       <a class="genric-btn primary" href="actions/carrito.php?id=<?php echo $id;?>">+ Carrito</a>
                                        <a class="genric-btn primary-border" href="#">comprar</a>
                                   </div>
                               </div>
                           </div>
                           <?php }

                           ?>
                    </div>
                </div>
                <!-- card Remeras y Musculosas -->
                <div class="tab-pane fade" id="nav-remera" role="tabpanel" aria-labelledby="nav-remera-tab">
                        <div class="row">
                        <?php
                           
                           
                           $query = "SELECT * FROM `catalogo` WHERE `type` = 'remera' GROUP BY `title`";
                           $result= mysqli_query($conn, $query);  
                   
                           while($row = mysqli_fetch_assoc($result)) { 
                           
                           $id = $row['id'];
                           $title= $row['title'];
                           $description = $row['description'];
                           $imagen = $row['imagen'];
                           $precio_venta = $row['precio_venta'];
                           $oferta = $row['oferta'];
                           $rebaja_ok = $row['rebaja_ok'];
                           $type = $row['type'];
                       ?>
                           <div class="col-xl-4 col-lg-4 col-md-6">
                               <div class="single-product mb-60">
                                   <div class="product-img">
                                       <img src="<?php echo $imagen;?>" alt="">
                                       
                                   </div>
                                   <div class="product-caption">
                                       <!--div class="product-ratting">
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star low-star"></i>
                                           <i class="far fa-star low-star"></i>
                                       </div-->
                                       <h4><a href="#"><?php echo $title;?></a></h4>
                                       <div class="price">
                                           <ul>
                                               <li>$<?php echo $precio_venta;?></li>
                                               <!--li class="discount">$60.00</li-->
                                           </ul>
                                       </div>
                                       <a class="genric-btn primary" href="actions/carrito.php?id=<?php echo $id;?>">+ Carrito</a>
                                            <a class="genric-btn primary-border" href=".#">comprar</a>
                                   </div>
                               </div>
                           </div>
                           <?php }

                           ?>
                    </div>
                </div>
                <!-- card Camperas-->
                <div class="tab-pane fade" id="nav-campera" role="tabpanel" aria-labelledby="nav-campera-tab">
                        <div class="row">
                        <?php
                           
                           
                           $query = "SELECT * FROM `catalogo` WHERE `type` = 'campera' GROUP BY `title`";
                           $result= mysqli_query($conn, $query);  
                   
                           while($row = mysqli_fetch_assoc($result)) { 
                           
                           $id = $row['id'];
                           $title= $row['title'];
                           $description = $row['description'];
                           $imagen = $row['imagen'];
                           $precio_venta = $row['precio_venta'];
                           $oferta = $row['oferta'];
                           $rebaja_ok = $row['rebaja_ok'];
                           $type = $row['type'];
                       ?>
                           <div class="col-xl-4 col-lg-4 col-md-6">
                               <div class="single-product mb-60">
                                   <div class="product-img">
                                       <img src="<?php echo $imagen;?>" alt="">
                                       
                                   </div>
                                   <div class="product-caption">
                                       <!--div class="product-ratting">
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star low-star"></i>
                                           <i class="far fa-star low-star"></i>
                                       </div-->
                                       <h4><a href="#"><?php echo $title;?></a></h4>
                                       <div class="price">
                                           <ul>
                                               <li>$<?php echo $precio_venta;?></li>
                                               <!--li class="discount">$60.00</li-->
                                           </ul>
                                       </div>
                                       <a class="genric-btn primary" href="actions/carrito.php?id=<?php echo $id;?>">+ Carrito</a>
                                            <a class="genric-btn primary-border" href="#">comprar</a>
                                   </div>
                               </div>
                           </div>
                           <?php }

                           ?>
                    </div>
                </div><!-- card Accesorios-->
                    <div class="tab-pane fade" id="nav-accesorio" role="tabpanel" aria-labelledby="nav-accesorio-tab">
                        <div class="row">
                        <?php
                           
                           
                           $query = "SELECT * FROM `catalogo` WHERE `type` = 'accesorio' GROUP BY `title`";
                           $result= mysqli_query($conn, $query);  
                   
                           while($row = mysqli_fetch_assoc($result)) { 
                           
                           $id = $row['id'];
                           $title= $row['title'];
                           $description = $row['description'];
                           $imagen = $row['imagen'];
                           $precio_venta = $row['precio_venta'];
                           $oferta = $row['oferta'];
                           $rebaja_ok = $row['rebaja_ok'];
                           $type = $row['type'];
                       ?>
                           <div class="col-xl-4 col-lg-4 col-md-6">
                               <div class="single-product mb-60">
                                   <div class="product-img">
                                       <img src="<?php echo $imagen;?>" alt="">
                                       
                                   </div>
                                   <div class="product-caption">
                                       <!--div class="product-ratting">
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star"></i>
                                           <i class="far fa-star low-star"></i>
                                           <i class="far fa-star low-star"></i>
                                       </div-->
                                       <h4><a href="#"><?php echo $title;?></a></h4>
                                       <div class="price">
                                           <ul>
                                               <li>$<?php echo $precio_venta;?></li>
                                               <!--li class="discount">$60.00</li-->
                                           </ul>
                                       </div>
                                       <a class="genric-btn primary" href="actions/carrito.php?id=<?php echo $id;?>">+ Carrito</a>
                                            <a class="genric-btn primary-border" href="#">comprar</a>
                                   </div>
                               </div>
                           </div>
                           <?php }

                           ?>
                    </div>
                </div>
                <!-- End Nav Card -->
            </div>
        </section>
        <!-- Latest Products End -->
        <!-- Latest Offers Start -->
        <div class="latest-wrapper lf-padding">
            <div class="latest-area latest-height d-flex align-items-center" data-background="assets/img/collection/latest-offer.png">
                <div class="container">
                    <div class="row d-flex align-items-center">
                        <div class="col-xl-5 col-lg-5 col-md-6 offset-xl-1 offset-lg-1">
                            <div class="latest-caption">
                                <h2>Seguinos en</h2>
                                <p>Instagram</p>
                            </div>
                        </div>
                            <div class="col-xl-5 col-lg-5 col-md-6 ">
                            <div class="latest-subscribe">
                                <form action="#">
                                    <!--input type="email" placeholder="Your email here"-->
                                    <button href="https://www.instagram.com/vimindumentariadeportiva/"> <i class="fab fa-instagram"></i> @vimindumentaria</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- man Shape -->
                <div class="man-shape">
                    <img src="assets/img/collection/latest-man.png" alt="">
                </div>
            </div>
        </div>
        <!-- Latest Offers End -->
        <!-- Shop Method Start-->
        <div class="shop-method-area section-padding30">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-package"></i>
                            <h6>Envios</h6>
                            <p>llegamos a todo el pais por Correo Argentino. </p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-credit-card"></i>
                            <h6>Metodos de Pago</h6>
                            <p>Mercado Pago + todas las tarjetas.</p>
                        </div>
                    </div> 
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-method mb-40">
                            <i class="ti-home"></i>
                            <h6>Mendoza Rosario</h6>
                            <p>Coordiná cono nosotros la entrega.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Shop Method End-->
        <!-- Gallery Start-->
        <div class="gallery-wrapper lf-padding">
            <div class="gallery-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="gallery-items">
                            <img src="assets/img/VIM/men.JPG"  style="height: 22rem; " alt="">
                        </div> 
                        <div class="gallery-items">
                            <img src="assets/img/VIM/tou.JPG"  style="height: 22rem; "  alt="">
                        </div>
                        <div class="gallery-items">
                            <img src="assets/img/VIM/medio.JPG" style="height: 22rem; "  alt="">
                        </div>
                        <div class="gallery-items">
                            <img src="assets/img/VIM/che.JPG"  style="height: 22rem; " alt="">
                        </div>
                        <div class="gallery-items">
                            <img src="assets/img/VIM/girl.JPG" style="height: 22rem; " alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Gallery End-->

    </main>
   <footer>
       <!-- Footer Start-->
       <div class="footer-area footer-padding">
           <div class="container">
               <div class="row d-flex justify-content-between">
                   <div class="col-xl-3 col-lg-3 col-md-5 col-sm-6">
                      <div class="single-footer-caption mb-50">
                        <div class="single-footer-caption mb-30">
                             <!-- logo -->
                            <div class="footer-logo">
                                <a href="index.html"><img src="assets/img/VIM/LOGO.JPG" style="height: 5rem;"alt=""></a>
                            </div>
                            <div class="footer-tittle">
                                <div class="footer-pera">
                                    <p>SONDER - TOUCHE</p>
                               </div>
                            </div>
                        </div>
                      </div>
                   </div>
                   <div class="col-xl-2 col-lg-3 col-md-3 col-sm-5">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>Catalogos</h4>
                               <ul>
                                   <li><a href="#">Gitl</a></li>
                                   <li><a href="#"> Mens</a></li>
                                   <li><a href="#"> Hot Sale</a></li>
                                   <li><a href="#"> Team</a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-4 col-sm-7">
                       <!--div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>New Products</h4>
                               <ul>
                                   <li><a href="#">Woman Cloth</a></li>
                                   <li><a href="#">Fashion Accessories</a></li>
                                   <li><a href="#"> Man Accessories</a></li>
                                   <li><a href="#"> Rubber made Toys</a></li>
                               </ul>
                           </div>
                       </div-->
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-5 col-sm-7">
                       <!--div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>Support</h4>
                               <ul>
                                <li><a href="#">Frequently Asked Questions</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Report a Payment Issue</a></li>
                            </ul>
                           </div>
                       </div>
                   </div-->
               </div>
               <!-- Footer bottom -->
               <div class="row">
                <div class="col-xl-7 col-lg-7 col-md-7">
                    <div class="footer-copy-right">
                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> Programada y Diseñada por BUILD.IT | This template is made with by Colorlib  
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>                 </div>
                </div>
                 <div class="col-xl-5 col-lg-5 col-md-5">
                    <div class="footer-copy-right f-right">
                        <!-- social -->
                        <div class="footer-social">
                            <a href="#"><i class="fab fa-twitter"></i></a>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-behance"></i></a>
                            <a href="#"><i class="fas fa-globe"></i></a>
                        </div>
                    </div>
                </div>
            </div>
           </div>
       </div>

       <!-- Footer End-->
   </footer>
   
	<!-- JS here -->
	
		<!-- All JS Custom Plugins Link Here here -->
        <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
		
		<!-- Jquery, Popper, Bootstrap -->
		<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="./assets/js/popper.min.js"></script>
        <script src="./assets/js/bootstrap.min.js"></script>
	    <!-- Jquery Mobile Menu -->
        <script src="./assets/js/jquery.slicknav.min.js"></script>

		<!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="./assets/js/owl.carousel.min.js"></script>
        <script src="./assets/js/slick.min.js"></script>

		<!-- One Page, Animated-HeadLin -->
        <script src="./assets/js/wow.min.js"></script>
		<script src="./assets/js/animated.headline.js"></script>
        <script src="./assets/js/jquery.magnific-popup.js"></script>

		<!-- Scrollup, nice-select, sticky -->
        <script src="./assets/js/jquery.scrollUp.min.js"></script>
        <script src="./assets/js/jquery.nice-select.min.js"></script>
		<script src="./assets/js/jquery.sticky.js"></script>
        
        <!-- contact js -->
        <script src="./assets/js/contact.js"></script>
        <script src="./assets/js/jquery.form.js"></script>
        <script src="./assets/js/jquery.validate.min.js"></script>
        <script src="./assets/js/mail-script.js"></script>
        <script src="./assets/js/jquery.ajaxchimp.min.js"></script>
        
		<!-- Jquery Plugins, main Jquery -->	
        <script src="./assets/js/plugins.js"></script>
        <script src="./assets/js/main.js"></script>
        
    </body>
</html>